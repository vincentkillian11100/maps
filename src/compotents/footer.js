import imgFooter from '../images/footer.svg'
function Footer() {
    return (
        <footer>
            <div className="logo_comtainerFooter">
                <img className='imgL' src={imgFooter} alt="" />
            </div>
            <div className="nav_containerFooter">
                <nav>
                    <ul className="ul_footer">
                        <li>Mentions l'égales</li>
                        <li>Nos actualités</li>
                        <li>Nous rejoindre</li>
                        <li>Contact</li>
                        <li>Presse</li>
                        <li>Plan du site</li>
                        <li>Notres agence numériques</li>
                    </ul>
                </nav>
            </div>
        </footer>
    )
}
export default Footer;