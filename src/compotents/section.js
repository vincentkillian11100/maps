import imgFooter from '../images/main.jpg'
function CreateSection() {
    return (
        <div className="sectionInfo">
            <div className="container_main">
                <div className="child_main_right backgoundImg">
                    <h1 className="simplon">SIMPLON.CO en Occitanie</h1>
                    <p>Simplon.co est un réseau de fabriques solidaires et inclusives qui proposent des formation gratuites aux métiers techniques du numérique en France et à l’étranger</p>
                    <div className="container_btn">
                        <div className="btmFormaction">FOMATION OUVERT</div>
                    </div>
                </div>
                <div className="child_main_left disparaision">
                    <img className="img_main" src={imgFooter} alt="j" />
                </div>
            </div>
        </div>
    )
}
export default CreateSection;