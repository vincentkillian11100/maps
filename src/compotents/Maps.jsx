
import '../styles/App.css';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import positions from '../tab.json';
import { useState } from 'react';
import Header from './header';
import CreateSection from './section';
import Footer from './footer';


function Maps() {
  const [data, setData] = useState(null)
  const [i, seti] = useState([0, 0]);
  navigator.geolocation.getCurrentPosition(function (position) {
    let a = position.coords.latitude;
    let b = position.coords.longitude;

    seti([a, b])

  })
  const [meteo, addMeteo] = useState(null);
  function AddMeteo(lat, lon) {

    let latText = 'lat=' + lat + '&'
    let longText = 'lon=' + lon + '&'
    let clé = 'appid=430851f055bc4a0eb16544f6c063c1aa&units=metric&lang=fr'
    let lien = 'https://api.openweathermap.org/data/2.5/weather?' + latText + longText + clé

    fetch(lien)
      .then(function (response) {
        return response.json()
      })
      .then(function (monjson) {
        addMeteo(monjson);
        console.log(monjson);
      });

  }
  return (
    <div className="containeGmobal">
      <Header />
      <CreateSection />
      <h1>Simplon près de chez vous</h1>
      <div className="container">
        <div id="mapRight">

          <div className="containerSimplon">
            <p className="nomSimplon">{data ? (data.name) : ''}</p>
            <div className="Position">
              <i className="fa-solid fa-location-dot loc"></i>
              <p className="adressseSimplon"> {data ? (data.adresse) : ''}</p>
            </div>
            <p className="foramationSimplon"> {data ? (data.formation) : ''}</p>
          </div>

        </div>
        <div id="map">
          <MapContainer center={[43.6487851, 2.3435684]} zoom={6}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright%22%3EOpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {positions.map((ville) => (
              <Marker
                position={[ville.longitude, ville.latitude]}
                key={ville.name}
                eventHandlers={{
                  click: () => {
                    setData(ville);
                    AddMeteo(ville.longitude, ville.latitude);

                  },
                }}>

                <Popup>
                  <div id={ville.name}>
                    <p className="nomPop">{ville.name + ' '}</p>
                    <div className='containerPopup'>
                    <div>
                    <p>{meteo?.main.temp}°C</p>
                    <p>{(meteo?.weather) ? meteo?.weather[0]?.description : ""}</p>
                    </div>
                    <div>
                    <p>Humidité {(meteo) ? meteo.main.humidity : ""}%</p>
                    <p>Vent {(meteo) ? meteo.wind.speed : ""}Km/h</p>
                    </div>
                    </div>
                  </div>
                </Popup>

              </Marker>
            ))}

            <Marker position={i}
              eventHandlers={{
                click: () => {
                  AddMeteo(i[0], i[1]);
                },
              }}>
              <Popup>
                <p className="nomPop">{meteo?.name}</p>
                <p>{meteo?.weather[0]?.description}</p>
                <p>{meteo?.main.temp}°C</p>
                <p>Humidité {(meteo) ? meteo.main.humidity : ""} %</p>
                <p>Vent {(meteo) ? meteo.wind.speed : " "} Km/h</p>
              </Popup>
            </Marker>
          </MapContainer>
        </div>
      </div>
      <Footer />
    </div>
  );

}


export default Maps;
